package com.restkeeper.utils;

import lombok.Getter;

/**
 * 备注枚举
 */
@Getter
public enum RemarkType {

    RETRUN_DISH_REASONS(1,"退菜原因"),
    RETRUN_ORDER_REASONS(2,"退单原因"),
    CANCEL_ORDER_REASONS(3,"撤单原因"),
    REVERSE_ORDER_REASONS(4,"反结账原因"),
    DISCOUNT_REASONS(5,"打折原因"),
    PRESENT_DISH_REASONS(6,"赠菜原因"),
    FREE_ORDER_REASONS(7,"免单原因"),
    WHOLE_ORDER_REMARKS(8,"整单备注"),
    LEAFLET_ORDER_REMARKS(9,"传单备注");

    private Integer type;
    private String remark;

    public static String getRemark(Integer type){
        RemarkType[] RemarkEnums=  RemarkType.values();
        for (RemarkType remarkEnum : RemarkEnums) {
            if(remarkEnum.getType().equals(type)){
                return remarkEnum.getRemark();
            }
        }
        return "";
    }

    RemarkType(Integer type, String remark) {
        this.type = type;
        this.remark = remark;
    }
}
