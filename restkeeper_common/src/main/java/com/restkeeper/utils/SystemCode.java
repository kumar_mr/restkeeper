package com.restkeeper.utils;

/**
 * 全局变量
 */
public class SystemCode {
    // 口味描述
    public final static String DICTIONARY_FLAVOR ="flavor";

    public  final  static  String DICTIONARY_REMARK="remark"; //字典表备注类型

    //运营端账号下发队列
    public final static String SMS_ACCOUNT_QUEUE="account_queue";

    //点餐 加菜队列
    public final static String DISH_QUEUE="dish_queue";

    // 小程序端通信交换机名称
    public final static String MICRO_APP_EXCHANGE = "micro_app_exchange";

    public final static String PRINTER_EXCHANGE_NAME = "print.app.exchange";


   //1 集团类型  2 门店类型
    public  final  static String  USER_TYPE_SHOP="1"; //集团用户类型
    public  final  static String  USER_TYPE_STORE_MANAGER="2"; //门店管理员类型
    public  final  static String  USER_TYPE_STAFF="3";  //普通员工
    public  final  static String  USER_TYPE_H5="4";  //h5 类型用户

    //禁用
    public  final  static int  FORBIDDEN=0;
    //开启
    public  final  static int  ENABLED=1;

    public  final  static int  DISH_TYPE_MORMAL=1; //普通菜品
    public  final  static int  DISH_TYPE_SETMEAL=2; //套餐

    // 挂账类型：1 公司 2 个人
    public final static int CREDIT_TYPE_COMPANY = 1;
    public final static int CREDIT_TYPE_USER= 2;

    public  final  static int  TABLE_STATUS_FREE=0; // 0空闲
    public  final  static int  TABLE_STATUS_OPEND=1; // 1 开桌
    public  final  static int  TABLE_STATUS_LOCKED=2; //2 锁定


    // 订单状态 0：未付 1：已付 2:支付中
    public final static int ORDER_STATUS_NOPAY = 0;
    public final static int ORDER_STATUS_PAYED = 1;
    public final static int ORDER_STATUS_PAYING = 2;

    // 订单来源 0 门店 1 app
    public final static int ORDER_SOURCE_STORE = 0;
    public final static int ORDER_SOURCE_APP = 1;

    //小程序前缀字符串
    public final static String MIRCO_APP_SHOP_CART_PREFIX = "micro.app.shopCart.";

    //小程序支付时锁前缀字符串
    public final static String MICRO_APP_LOCK_PREFIX = "micro.app.order.";

    // 打印类型 0:后厨制作菜单 1:后厨转菜单 2:后厨转台单 3:后厨退菜单 4:前台预结单 5:结账单 6:客单
    public final static int PRINT_MADE_MENU = 0;
    public final static int PRINT_CHANGE_MENU = 1;
    public final static int PRINT_CHANGE_TABLE = 2;
    public final static int PRINT_RETURN_DISH = 3;
    public final static int PRINT_BEFOREHAND = 4;
    public final static int PRINT_BILL = 5;
    public final static int PRINT_CUSTOMER = 6;
}
