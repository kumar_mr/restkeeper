package com.restkeeper.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * 备注通用解析工具
 */
public class RemarkStringUtils {


    private List<String> getRemarkList(String remark){
        if(StringUtils.isNoneBlank(remark)){
            remark=remark.substring(remark.indexOf("[")+1,remark.indexOf("]"));
            if(StringUtils.isNotEmpty(remark)){
                String[] orderRemark_array= remark.split(",");
                return  Arrays.asList(orderRemark_array);
            }
        }
        return  Lists.newArrayList();
    }


}
