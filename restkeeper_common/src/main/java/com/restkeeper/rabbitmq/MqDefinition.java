package com.restkeeper.rabbitmq;

/**
 * rabbitMQ相关定义
 */
public class MqDefinition{
    /**
     * 路由键定义
     */
    public final static String ORDER_PAY_ROUTING_KEY = "order.pay.routing";
    /**
     * 队列名定义
     */
    public final static String ORDER_PAY_QUEUE_NAME = "pay.order.queue";
    /**
     * 交换机名称定义
     */
    public final static String ORDER_PAY_EXCHANGE_NAME = "pay.order.exchange";
}
