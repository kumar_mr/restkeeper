package com.restkeeper.dto;

import lombok.Data;

import java.util.List;

/**
 * 退菜，赠菜 订单详情封装类，方便以后扩展
 */
@Data
public class DetailDTO {
    private String detailId;
    private List<String> remarks;
}
