package com.restkeeper.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ReceiptDTO {

    private long page;
    private long pageSize;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String orderNumber;
    private String operatorName;
    private Integer payType;


}
