package com.restkeeper.dto;

import lombok.Data;

@Data
public class DishDTO{
    private String dishId;
    private int dishType;
    private int dishNumber;
    private String flavorRemark;
}
