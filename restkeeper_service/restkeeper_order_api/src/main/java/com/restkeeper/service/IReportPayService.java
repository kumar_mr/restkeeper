package com.restkeeper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.dto.CurrentAmountCollectDTO;
import com.restkeeper.dto.DayAmountCollectDTO;
import com.restkeeper.dto.PayTypeCollectDTO;
import com.restkeeper.dto.PrivilegeDTO;
import com.restkeeper.entity.ReportPayEntity;

import java.time.LocalDate;
import java.util.List;

public interface IReportPayService extends IService<ReportPayEntity>{
    /**
     * 获取一段日期内的销售趋势
     * @return
     */
    List<DayAmountCollectDTO> getDayAmountCollect(LocalDate start,LocalDate end);

    /**
     * 获取一定日期内的收款构成
     * @param start
     * @param end
     * @return
     */
    List<PayTypeCollectDTO> getPayTypeCollect(LocalDate start,LocalDate end);

    /**
     * 获取一定日期内的优惠统计
     * @param start
     * @param end
     * @return
     */
    PrivilegeDTO getPrivilegeCollectByDate(LocalDate start,LocalDate end);

    /**
     * 日报-销售数据汇总
     * @param start
     * @param end
     * @return
     */
    CurrentAmountCollectDTO getDateAmount(LocalDate start, LocalDate end);
}
