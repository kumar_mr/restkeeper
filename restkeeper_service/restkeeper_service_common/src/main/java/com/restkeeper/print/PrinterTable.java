package com.restkeeper.print;

import com.beust.jcommander.internal.Lists;

import java.util.List;

public class PrinterTable{
    private List<PrinterRow> rows = Lists.newArrayList();

    public void addRow(PrinterRow row){
        rows.add(row);
    }

    public List<PrinterRow> getRows(){
        return rows;
    }
}
