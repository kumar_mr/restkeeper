package com.restkeeper.print;

import lombok.Data;

import java.util.List;

@Data
public class PrintOrder{
    private String orderId;
    private String orderNumber;
    private Integer personNumber;
    private String createTime;
    private String billTime;
    private List<PrinterDish> dishList;
    private Integer payAmount;
}
