package com.restkeeper.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.entity.OperatorUser;
import com.restkeeper.utils.Result;

/**
 * <p>
 * 运营端管理员 服务类
 * </p>
 *
 */
public interface IOperatorUserService extends IService<OperatorUser> {


    IPage<OperatorUser>  queryPageByName(int pageNo, int pageSise, String name);

    Result login(String loginname, String loginpass);

}
