package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.shop.entity.Brand;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 品牌管理 服务类
 * </p>
 */
public interface IBrandService extends IService<Brand> {

    public IPage<Brand> queryPage(Long pageNo, Long pageSise);

    public List<Map<String, String>> brandList();
}
