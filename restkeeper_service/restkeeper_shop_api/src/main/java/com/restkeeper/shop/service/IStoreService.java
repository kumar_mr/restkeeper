package com.restkeeper.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.shop.dto.StoreDTO;
import com.restkeeper.shop.entity.Store;

import java.util.List;

/**
 * <p>
 * 门店信息账号 服务类
 * </p>
 */
public interface IStoreService extends IService<Store> {

    /**
     * 支持分页及模糊查询门店信息
     * @param pageNo
     * @param pageSise
     * @param name 门店名称
     * @return
     */
    public IPage<Store> queryPageByName(long pageNo, long pageSise, String name);

    /**
     * 获取所有省份信息
     * @return
     */
    public List<String> listAllProvince();

    /**
     * 根据省份获取门店信息
     * @return
     */
    public List<StoreDTO> getStoreByProvince(String province);


    /**
     * 商户下管理的门店
     * @return
     */
    public List<StoreDTO> listManagerStores();


}
