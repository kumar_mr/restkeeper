package com.restkeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.entity.ReportTime;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ReportTimeMapper extends BaseMapper<ReportTime> {
}
