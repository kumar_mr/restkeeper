package com.restkeeper.config;

import com.restkeeper.rabbitmq.MqDefinition;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig{
    /**
     * 创建队列
     * @return
     */
    @Bean(name = "payOrderQueue")
    public Queue payOrderQueue() {
        return new Queue(MqDefinition.ORDER_PAY_QUEUE_NAME,true);
    }

    /**
     * 创建交换机
     * @return
     */
    @Bean(name = "payOrderExchange")
    public TopicExchange payOrderExchange() {
        return new TopicExchange(MqDefinition.ORDER_PAY_EXCHANGE_NAME,true,false);
    }

    /**
     * 将队列和交换机绑定
     * @param toVmQueue
     * @param exchange
     * @return
     */
    @Bean
    public Binding bindingPayOrderExchangeMessage(
            @Qualifier("payOrderQueue") Queue toVmQueue,
            @Qualifier("payOrderExchange") TopicExchange exchange) {
        return BindingBuilder.bind(toVmQueue).to(exchange).with(MqDefinition.ORDER_PAY_ROUTING_KEY);
    }
}
