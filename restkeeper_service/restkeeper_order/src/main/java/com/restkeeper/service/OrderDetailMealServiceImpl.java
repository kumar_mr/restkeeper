package com.restkeeper.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.entity.OrderDetailMealEntity;
import com.restkeeper.mapper.OrderDetailMealMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Service(version = "1.0.0", protocol = "dubbo")
@Component
public class OrderDetailMealServiceImpl extends ServiceImpl<OrderDetailMealMapper, OrderDetailMealEntity> implements IOrderDetailMealService{
}
