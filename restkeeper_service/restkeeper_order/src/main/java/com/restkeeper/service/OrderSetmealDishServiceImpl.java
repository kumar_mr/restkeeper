package com.restkeeper.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.restkeeper.dto.OrderMsgDTO;
import com.restkeeper.entity.OrderDetailEntity;
import com.restkeeper.entity.OrderDetailMealEntity;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.service.ISetmealDishService;
import com.restkeeper.tenant.TenantContext;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Service(version = "1.0.0", protocol = "dubbo")
public class OrderSetmealDishServiceImpl implements IOrderSetmealDishService{
    @Autowired
    private IOrderDetailService orderDetailService;
    @Autowired
    private IOrderDetailMealService orderDetailMealService;
    @Reference(version = "1.0.0", check=false)
    private ISetmealDishService setmealDishService;
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void process(String msg) {
        OrderMsgDTO msgObj = JSON.parseObject(msg, OrderMsgDTO.class);
        String orderId = msgObj.getOrderId();
//        RpcContext.getContext().setAttachment("shopId",msgObj.getShopId());
//        RpcContext.getContext().setAttachment("storeId",msgObj.getStoreId());

        TenantContext.addAttachment("shopId",msgObj.getShopId());
        TenantContext.addAttachment("storeId",msgObj.getStoreId());
        QueryWrapper<OrderDetailEntity> wrapper = new QueryWrapper<>();
        //从订单详情里查出所有套餐
        wrapper.lambda()
                .eq(OrderDetailEntity::getOrderId,orderId)
                .eq(OrderDetailEntity::getDishType,2);
        orderDetailService.list(wrapper)
                .forEach(o->{
                    float payPrice = o.getDishAmount();
                    int dishNumber = o.getDishNumber();
                    String setmealId = o.getDishId();
                    //通过套餐详情查出所有菜品
                    List<Dish> dishList = setmealDishService.getAllDishBySetmealId(setmealId);
                    float allDishPrice = dishList.stream().map(d->d.getPrice()*setmealDishService.getDishCopiesInSetmeal(d.getId(),setmealId)).reduce(Integer::sum).get()*o.getDishNumber();
                    float rate = payPrice/allDishPrice;
                    OrderDetailMealEntity orderDetailMealEntity = new OrderDetailMealEntity();
                    BeanUtils.copyProperties(o,orderDetailMealEntity);
                    //根据菜品生成菜品订单套餐详情
                    dishList.forEach(d->{
                        int copies = setmealDishService.getDishCopiesInSetmeal(d.getId(),setmealId);
                        orderDetailMealEntity.setDetailId(null);
                        orderDetailMealEntity.setDishId(d.getId());
                        orderDetailMealEntity.setDishName(d.getName());
                        orderDetailMealEntity.setDishNumber(dishNumber*copies);
                        orderDetailMealEntity.setDishAmount((int)(d.getPrice()*dishNumber*copies*rate));
                        orderDetailMealEntity.setDishType(1);
                        orderDetailMealEntity.setDishPrice(d.getPrice());
                        orderDetailMealEntity.setShopId(null);
                        orderDetailMealEntity.setStoreId(null);
                        orderDetailMealService.getBaseMapper().insert(orderDetailMealEntity);
                    });
                });
    }
}
