package com.restkeeper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.restkeeper.dto.CurrentHourCollectDTO;
import com.restkeeper.entity.ReportTime;
import com.restkeeper.mapper.ReportTimeMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.List;

@Service(version = "1.0.0",protocol = "dubbo")
public class ReportTimeServiceImpl implements IReportTimeService {

    @Autowired
    private ReportTimeMapper reportTimeMapper;

    @Override
    public List<CurrentHourCollectDTO> getDateHourCollect(LocalDate start, Integer type) {

        List<CurrentHourCollectDTO> result = Lists.newArrayList();

        QueryWrapper<ReportTime> wrapper = new QueryWrapper<>();

        if (1 == type){
            //按销售额汇总
            wrapper.select("SUM(total_amount) as total_amount","pay_time")
                    .lambda()
                    .eq(ReportTime::getPayDate,start)
                    .groupBy(ReportTime::getPayTime);
        }

        if (2 == type){
            //按单数汇总
            wrapper.select("SUM(total_count) as total_count","pay_time")
                    .lambda()
                    .eq(ReportTime::getPayDate,start)
                    .groupBy(ReportTime::getPayTime);
        }

        List<ReportTime> reportTimeList = reportTimeMapper.selectList(wrapper);

        reportTimeList.forEach(r->{
            CurrentHourCollectDTO currentHourCollectDTO = new CurrentHourCollectDTO();
            if (1 == type){
                currentHourCollectDTO.setTotalAmount(r.getTotalAmount());
            }
            if (2 == type){
                currentHourCollectDTO.setTotalAmount(r.getTotalCount());
            }
            currentHourCollectDTO.setCurrentDateHour(r.getPayTime());
            result.add(currentHourCollectDTO);
        });

        return result;
    }
}
