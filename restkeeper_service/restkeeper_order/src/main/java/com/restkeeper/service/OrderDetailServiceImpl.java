package com.restkeeper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.entity.OrderDetailAllView;
import com.restkeeper.entity.OrderDetailEntity;
import com.restkeeper.mapper.OrderDetailAllMapper;
import com.restkeeper.mapper.OrderDetailMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 订单详情表 服务实现类
 * </p>
 */
@Service(version = "1.0.0", protocol = "dubbo")
@Component
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetailEntity> implements IOrderDetailService {
    @Autowired
    private OrderDetailAllMapper orderDetailAllMapper;
    @Override
    public List<OrderDetailAllView> getCurrentCategoryAmountCollect(LocalDate start,LocalDate end) {
        QueryWrapper<OrderDetailAllView> wrapper = new QueryWrapper<>();
        wrapper.select("dish_category_name","sum(dish_amount) as dish_amount")
                .lambda()
                .ge(OrderDetailAllView::getLastUpdateTime,start)
                .lt(OrderDetailAllView::getLastUpdateTime,end)
                .eq(OrderDetailAllView::getDishType,1)
                .groupBy(OrderDetailAllView::getDishCategoryName);

        return orderDetailAllMapper.selectList(wrapper);
    }

    @Override
    public List<OrderDetailAllView> getCurrentCategoryCountCollect(LocalDate start,LocalDate end) {
        QueryWrapper<OrderDetailAllView> wrapper = new QueryWrapper<>();
        wrapper.select("dish_category_name","count(dish_category_name) as total_count")
                .lambda()
                .ge(OrderDetailAllView::getLastUpdateTime,start)
                .lt(OrderDetailAllView::getLastUpdateTime,end)
                .eq(OrderDetailAllView::getDishType,1)
                .groupBy(OrderDetailAllView::getDishCategoryName);

        return orderDetailAllMapper.selectList(wrapper);
    }

    @Override
    public List<OrderDetailAllView> getCurrentDishRank(LocalDate start,LocalDate end) {
        QueryWrapper<OrderDetailAllView> wrapper = new QueryWrapper<>();
        wrapper.select("dish_name","sum(dish_number) as total_count")
                .lambda()
                .ge(OrderDetailAllView::getLastUpdateTime,start)
                .lt(OrderDetailAllView::getLastUpdateTime,end)
                .eq(OrderDetailAllView::getDishType,1)
                .groupBy(OrderDetailAllView::getDishName);
        wrapper.orderByDesc("total_count");
        List<OrderDetailAllView> result = orderDetailAllMapper.selectList(wrapper);

        return result;
    }

    @Override
    public List<OrderDetailEntity> getDetailsByOrderId(String orderId) {
        QueryWrapper<OrderDetailEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(OrderDetailEntity::getOrderId,orderId);
        return this.list(queryWrapper);
    }
}
