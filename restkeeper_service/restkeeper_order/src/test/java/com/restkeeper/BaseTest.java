package com.restkeeper;

import com.restkeeper.tenant.TenantContext;
import com.restkeeper.utils.SystemCode;
import org.apache.dubbo.rpc.RpcContext;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseTest {

    @Before
    public void init(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("userType", SystemCode.USER_TYPE_STAFF);
        map.put("shopId","test");
        map.put("storeId","1205130634395324417");
        map.put("logUserId","111");
        TenantContext.addAttachments(map);
        RpcContext.getContext().setAttachment("userType",SystemCode.USER_TYPE_STAFF);
        RpcContext.getContext().setAttachment("shopId","test");
        RpcContext.getContext().setAttachment("storeId","1205130634395324417");
        RpcContext.getContext().setAttachment("logUserId","111");
    }
}
