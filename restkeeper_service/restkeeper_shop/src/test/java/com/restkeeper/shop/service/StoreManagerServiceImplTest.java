package com.restkeeper.shop.service;

import com.restkeeper.utils.Result;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;

public class StoreManagerServiceImplTest extends  BaseTest {

    @Reference(version = "1.0.0", check=false)
    private IStoreManagerService storeManagerService;

    @Test
    public  void testlogin(){
        Result result= storeManagerService.login("test","18810973345","admin");
        System.out.println(result);
    }
}
