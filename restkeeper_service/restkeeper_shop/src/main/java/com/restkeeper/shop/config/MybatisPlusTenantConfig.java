package com.restkeeper.shop.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.tenant.TenantHandler;
import com.baomidou.mybatisplus.extension.plugins.tenant.TenantSqlParser;
import com.google.common.collect.Lists;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.List;

@Configuration
public class MybatisPlusTenantConfig {

    private static final String SYSTEM_TENANT_ID = "shop_id";
    private static final List<String> IGNORE_TENANT_TABLES = Lists.newArrayList("");

    @Bean
    @Primary
    public PaginationInterceptor paginationInterceptor() {
    	System.out.println("paginationInterceptor---");
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        // SQL解析处理拦截：增加租户处理回调。
        TenantSqlParser tenantSqlParser = new TenantSqlParser()
                .setTenantHandler(new TenantHandler() {
					@Override
					public Expression getTenantId(boolean where) {
                      // 从当前系统上下文中取出当前请求的服务商ID，通过解析器注入到SQL中。
                      String shopId = RpcContext.getContext().getAttachment("shopId");
                      if (null == shopId) {
                          throw new RuntimeException("#1129 getCurrentProviderId error.");
                      }
                      return new StringValue(shopId);
					}

					@Override
					public String getTenantIdColumn() {
						  return SYSTEM_TENANT_ID;
					}

					@Override
					public boolean doTableFilter(String tableName) {
                      // 忽略掉一些表：如租户表（provider）本身不需要执行这样的处理。
                      return IGNORE_TENANT_TABLES.stream().anyMatch((e) -> e.equalsIgnoreCase(tableName));
					}
                });
        paginationInterceptor.setSqlParserList( Lists.newArrayList(tenantSqlParser));
        return paginationInterceptor;
    }
}
