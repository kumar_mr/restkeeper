package com.restkeeper.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.redis.MybatisRedisCache;
import com.restkeeper.shop.entity.Brand;
import org.apache.ibatis.annotations.*;

/**
 * <p>
 * 品牌管理 Mapper 接口
 * </p>
 */
@Mapper
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface BrandMapper extends BaseMapper<Brand> {

}
