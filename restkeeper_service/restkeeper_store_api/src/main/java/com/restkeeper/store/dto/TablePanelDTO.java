package com.restkeeper.store.dto;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.store.entity.Table;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class TablePanelDTO {
    @ApiModelProperty(value = "空闲统计")
    private int freeCount;

    @ApiModelProperty(value = "锁定状态统计")
    private int lockedCount;

    @ApiModelProperty(value = "已开台")
    private int openedCount;

    @ApiModelProperty(value = "桌台信息")
    IPage<Table> tablePage;
}
