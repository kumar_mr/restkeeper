package com.restkeeper.store.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.SetmealDish;

import java.util.List;

public interface ISetmealDishService extends IService<SetmealDish>{
    boolean add(SetmealDish setmealDish);

    List<String> selectSetMealIdsByDishId(String dishId);

    List<String> selectDishsBySetMeal(String dishId);

    List<Dish> getAllDishBySetmealId(String setmealId);

    List<String> selectSetMealIdsByDishId(List<String> ids);

    Integer getDishCopiesInSetmeal(String dishId,String setmealId);
}
