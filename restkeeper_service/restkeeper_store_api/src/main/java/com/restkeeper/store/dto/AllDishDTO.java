package com.restkeeper.store.dto;

import lombok.Data;

@Data
public class AllDishDTO {

    private  String id;
    private  String name;
    private  Integer price;
    private  Integer type;
}
