package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.print.PrintOrder;
import com.restkeeper.store.entity.Printer;

import java.util.List;

public interface IPrinterService extends IService<Printer>{
    /**
     * 前台打印
     * @param
     */
    void frontPrint(PrintOrder printerOrder, int printType,String storeId,String shopId);

    /**
     * 后厨打印
     * @param dishId
     */
    void backendPrint(String dishId,int printType,int dishNumber,String storeId,String shopId);

    /**
     * 添加前台打印机
     * @param printer
     * @return
     */
    boolean addFrontPrinter(Printer printer);

    /**
     * 添加后厨打印机
     * @param printer
     * @return
     */
    boolean addBackendPrinter(Printer printer, List<String> dishIdList);

    /**
     * 获取打印机列表
     * @param pageNo
     * @param pageSize
     * @return
     */
    IPage<Printer> queryPage(Long pageNo, Long pageSize);

    /**
     * 更新打印机关联菜品
     * @param
     * @param dishIdList
     * @return
     */
    boolean updatePrinter(Printer printer,List<String> dishIdList);
}
