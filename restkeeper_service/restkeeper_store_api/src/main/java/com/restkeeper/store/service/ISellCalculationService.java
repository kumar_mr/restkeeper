package com.restkeeper.store.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.restkeeper.store.entity.SellCalculation;

/**
 * <p>
 * 沽清 服务类
 * </p>
 */
public interface ISellCalculationService extends IService<SellCalculation> {

    /**
     * 获取估清数目
     * @param dishId
     * @return
     */
    Integer getRemainderCount(String dishId);

    /**
     * 加菜时候 估清数目-1
     * @param dishId
     */
    boolean plusDish(String dishId);

    /**
     * 减菜时候 沽清剩余数量 +1
     * @param dishId
     * @return
     */
    boolean reduceDish(String dishId);

    /**
     * 加菜
     * @param dishId
     * @param count
     * @return
     */
    void add(String dishId, Integer count);

    /**
     * 减菜
     * @param dishId
     * @param count
     * @return
     */
    void decrease(String dishId, Integer count);



//    Integer getRemainderCount(String dishId,int dishType);
//    boolean add(int count,String dishId,int dishType);
//    boolean decrease(int count,String dishId,int dishType);
}
