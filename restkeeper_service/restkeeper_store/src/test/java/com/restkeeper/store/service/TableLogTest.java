package com.restkeeper.store.service;

import com.restkeeper.store.entity.Staff;
import com.restkeeper.store.entity.Table;
import com.restkeeper.store.entity.TableLog;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TableLogTest extends  BaseTest {

    @Reference(version = "1.0.0", check=false)
    private ITableLogService tableLogService;

    @Test
    public  void test(){
        TableLog tableLog =new TableLog();
        tableLog.setTableId("1");
        tableLog.setTableStatus(1);
        tableLog.setUserNumbers(10);
        tableLogService.save(tableLog);
    }

}
