package com.restkeeper.store.service;

import com.restkeeper.store.entity.Staff;
import com.restkeeper.tenant.TenantContext;
import com.restkeeper.utils.SystemCode;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.RpcContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

public class StaffTest extends  BaseTest{

    @Autowired
    private IStaffService staffService;


    @Test
    public  void test(){
        Staff staff =new Staff();
        staff.setStaffName("test");
        staff.setPhone("18840215564");
        staff.setPassword("test");
        staff.setIdNumber("111");
        staff.setSex("男");
        staffService.save(staff);
    }

}
