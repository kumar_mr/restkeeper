package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.restkeeper.store.dto.AllDishDTO;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.entity.SetmealDish;
import com.restkeeper.store.mapper.DishMapper;
import com.restkeeper.store.mapper.SetmealDishMapper;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DishMapperTest extends  BaseTest {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private ISetmealService setmealService;

    @Autowired
    SetmealDishMapper setmealDishMapper;


    /**
     * 自定义sql分页查询
     */
    @Test
    public void selectByMyPage() {
        QueryWrapper<Dish> wrapper = new QueryWrapper();
        //wrapper.like("name", "雨").lt("age", 40);
        Page<AllDishDTO> page = new Page<>(1,2);
        IPage<AllDishDTO> mapIPage = dishMapper.selectAllDish(page);

        System.out.println("总页数"+mapIPage.getPages());
        System.out.println("总记录数"+mapIPage.getTotal());
        List<AllDishDTO> records = mapIPage.getRecords();
        records.forEach(System.out::println);
    }

    @Test
    public void selectByMyPage2() {
        setmealDishMapper.selectById("1253505381044396033");
//        List<SetmealDish> dishList =setmealDishMapper.selectDishs("1253505380973092866");
//        for (SetmealDish setmealDish : dishList) {
//            System.out.println(setmealDish.getDish());
//        }
//        Setmeal setmeal = setmealService.getById("1253505380973092866");
//        List<SetmealDish> dishlist = setmeal.getDishList();
//        for (SetmealDish setmealDish : dishlist) {
//            System.out.println(setmealDish.getDish());
//        }
    }


}
