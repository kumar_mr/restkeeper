package com.restkeeper.store.service;

import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.DishCategory;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DishServiceImpTest extends  BaseTest{

    @Reference(version = "1.0.0", check=false)
    private IDishCategoryService dishCategoryService;

    @Reference(version = "1.0.0", check=false)
    private IDishService dishService;

    @Reference(version = "1.0.0", check=false)
    private IDishFlavorService dishFlavorService;

//    @Test
//    public void testadd(){
//        DishCategory dishCategory= new DishCategory();
//        dishCategory.setName("凉菜");
//        dishCategory.setType(SystemCode.DISH_TYPE_MORMAL);
//        dishCategoryService.save(dishCategory);
//        Dish dish =new Dish();
//        dish.setCategoryId(dishCategory.getCategoryId());
//        dish.setDishName("黄瓜拌菜");
//        dish.setCode("hgbc");
//        dish.setCategoryId(dishCategory.getCategoryId());
//        dishService.save(dish);
//
//        DishFlavor flavor =new DishFlavor();
//        flavor.setDishId(dish.getDishId());
//        flavor.setFlavorName("甜");
//        List<String> valueList = new ArrayList<>();
//        valueList.add("加甜");
//        valueList.add("蜂蜜味");
//        flavor.setFlavorValue(valueList.toString());
//        dishFlavorService.save(flavor);
//    }


    @Test
    public void serch(){
        DishCategory category= dishCategoryService.getById("1210006774620602369");
        System.out.println("category--------------- "+ category);
//        List<Dish> dishes= category.getDishList();
//        for (Dish dish : dishes) {
//            System.out.println("dish1----------- "+dish);
//            System.out.println("dish2----------- "+dish.getCategory());
//            System.out.println("dish3----------- "+dish.getFlavorList());
//        }
    }


//    @Autowired
//    private IDishService dishService;


//    @Test()
//    public void queryPage() {
//        IPage<Dish> result = dishService.queryPage(1L,10L);
//
//        Dish dish = dishService.getById("1204688052770500610");
//
//        UpdateWrapper<Dish> uw = new UpdateWrapper<>();
//        uw.lambda()
//            .set(Dish::getDishName,"辣菜3")
//            .eq(Dish::getDishId,"1204688052770500610");
//        //dishService.update(uw);
//
//        Dish dish1 = new Dish();
//        dish1.setCategoryName("1204666548716961793");
//        dish1.setCode("ccc");
//        dish1.setDescription("ttttt");
//        dish1.setDishName("甜菜");
//        dish1.setImage("");
//        dish1.setPrice(1.4);
//        dish1.setState(0);
//        dishService.save(dish);
//        Assert.assertTrue(true);
//    }
}