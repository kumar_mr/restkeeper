package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.store.dto.TablePanelDTO;
import com.restkeeper.store.entity.Table;
import com.restkeeper.store.mapper.TableMapper;
import com.restkeeper.utils.SystemCode;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(version = "1.0.0",protocol = "dubbo")
@Component
public class TableServiceImpl extends ServiceImpl<TableMapper, Table> implements ITableService{

//    @Reference(version = "1.0.0", check=false)
//    private IOrderService orderService;

    @Autowired
    ITableAreaService tableAreaService;

    @Autowired
    ITableLogService tableLogService;

    @Override
    public boolean add(Table table) {
        checkNameExsis(table.getTableName());
        return this.save(table);
    }

    /**
     * 桌台名称去重判断
     * @param name
     */
    private void checkNameExsis(String name){
        QueryWrapper<Table> qw = new QueryWrapper<>();
        qw.lambda()
                .select(Table::getTableId)
                .eq(Table::getTableName,name);
        Integer count = this.baseMapper.selectCount(qw);
        if(count > 0) throw new BussinessException("该桌台已存在");
    }

    @Override
    public IPage<Table> queryPage(long pageNo, long pageSize) {
        IPage<Table> page = new Page<>(pageNo,pageSize);
        QueryWrapper<Table> qw = new QueryWrapper<>();
        qw.lambda()
            .orderByDesc(Table::getLastUpdateTime);

        return this.page(page,qw);
    }

    @Override
    public IPage<Table> queryPageByArea(String areaId, long pageNo, long pageSize) {
        IPage<Table> page = new Page<>(pageNo,pageSize);
        QueryWrapper<Table> qw = new QueryWrapper<>();
        if (!areaId.equals("all")){
            qw.lambda().eq(Table::getAreaId,areaId);
        }
        page= this.page(page,qw);
        page.getRecords().forEach(d->{
            d.setArea(tableAreaService.getById(d.getAreaId()));
        });
        return page;
    }

//    @Override
//    public long getCountByAreaId(String areaId) {
//        QueryWrapper<Table> qw = new QueryWrapper<>();
//        qw.lambda()
//            .eq(Table::getAreaId,areaId);
//        return this.count(qw);
//    }

    @Override
    public TablePanelDTO getTablePanel(String areaId) {
        QueryWrapper<Table> qw = new QueryWrapper<>();
        qw.lambda()
                .eq(Table::getAreaId, areaId)
                .orderByDesc(Table::getLastUpdateTime);
        List<Table> tableList = this.list(qw);

        TablePanelDTO tablePanel =new TablePanelDTO();
        tablePanel.setFreeCount(1);
        tablePanel.setLockedCount(1);
        tablePanel.setOpenedCount(1);
        return  tablePanel;
    }

    @Override
    public boolean changeTable(String orderId, String targetTableId) {
        return false;
    }

    /**
     * 青台
     * @param tableId
     * @return
     */
    @Override
    public boolean cleanTable(String tableId){
        Table table = this.getById(tableId);
        table.setStatus(SystemCode.TABLE_STATUS_FREE);
        return this.updateById(table);
    }


    public boolean lockTable(String tableId){
        Table table = this.getById(tableId);
        if(SystemCode.TABLE_STATUS_OPEND==table.getStatus()||SystemCode.TABLE_STATUS_LOCKED==table.getStatus()){
            throw new BussinessException("非空闲不能锁桌");
        }
        table.setStatus(SystemCode.TABLE_STATUS_LOCKED);
        return this.updateById(table);
    }

    public boolean releaseTable(String tableId){
        Table table = this.getById(tableId);
        table.setStatus(SystemCode.TABLE_STATUS_FREE);
        return this.updateById(table);
    }

    @Override
    public Integer countTableByStatus(String areaId, Integer status) {
        QueryWrapper<Table> qw = new QueryWrapper<>();
        if (!areaId.equals("all")){
            qw.lambda()
                    .eq(Table::getAreaId, areaId);
        }
        qw.lambda().eq(Table::getStatus,status);
        return this.count(qw);
    }
}
