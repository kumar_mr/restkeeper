package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.store.entity.Table;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TableMapper extends BaseMapper<Table>{

    @Select(value="select * from t_table where area_id=#{areaId} and is_deleted=0 order by last_update_time desc")
    public List<Table> selectTablesByArea(@Param("areaId") String areaId);
}
