package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.store.entity.TableLog;

/**
 * <p>
 * 桌台记录 Mapper 接口
 * </p>
 */
public interface TableLogMapper extends BaseMapper<TableLog> {

}
