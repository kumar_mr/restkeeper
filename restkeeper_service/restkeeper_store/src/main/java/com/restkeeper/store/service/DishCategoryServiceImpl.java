package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.DishCategory;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.mapper.DishCategoryMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜品及套餐分类 服务实现类
 * </p>
 */
@Component
@Service(version = "1.0.0",protocol = "dubbo")
public class DishCategoryServiceImpl extends ServiceImpl<DishCategoryMapper, DishCategory> implements IDishCategoryService{

    @Autowired
    private IDishService dishService;

    @Autowired
    private ISetmealService setmealService;

    @Override
    public List<Map<String, Object>> findCategoryList(Integer type) {
        QueryWrapper<DishCategory> queryWrapper = new QueryWrapper<>();
        if (type != null){
            queryWrapper.lambda().eq(DishCategory::getType,type);
        }
        queryWrapper.lambda().select(DishCategory::getCategoryId,DishCategory::getName,DishCategory::getType);
        return this.listMaps(queryWrapper);
    }

    @Override
    public List<DishCategory> getAllCategory() {
        QueryWrapper<DishCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .lambda()
                .orderByAsc(DishCategory::getLastUpdateTime);

        return this.list(queryWrapper);
    }

    @Override
    @Transactional
    public boolean add(String name,int type) {
        checkNameExsis(name);
        DishCategory dishCategory = new DishCategory();
        dishCategory.setName(name);
        //默认排序0
        dishCategory.setTorder(0);
        dishCategory.setType(type);
        return this.save(dishCategory);
    }

    @Override
    public boolean update(String id, String categoryName) {
        checkNameExsis(categoryName);
        UpdateWrapper<DishCategory> uw = new UpdateWrapper<>();
        uw.lambda()
            .eq(DishCategory::getCategoryId,id)
            .set(DishCategory::getName,categoryName);
        return this.update(uw);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean delete(String id) {
        DishCategory dishCategory= this.getById(id);
        if(dishCategory==null){
            throw new BussinessException("分类不存在");
        }
        return this.removeById(id);
    }

    @Override
    public IPage<DishCategory> queryPage(Long pageIndex, Long pageSize) {
        IPage<DishCategory> page = new Page<>(pageIndex,pageSize);
        QueryWrapper<DishCategory> qw = new QueryWrapper<>();
        qw.lambda()
            .orderByDesc(DishCategory::getLastUpdateTime);
        return this.page(page);
    }

    /**
     * 检查类别名称是否存在，如果存在则抛出业务异常
     * @param name
     */
    private void checkNameExsis(String name){
        QueryWrapper<DishCategory> qw = new QueryWrapper<>();
        qw.lambda()
        .select(DishCategory::getCategoryId)
                .eq(DishCategory::getName,name);
        Integer count = this.baseMapper.selectCount(qw);
        if(count > 0) throw new BussinessException("该分类名称已存在");
    }

    @Override
    public List<DishCategory> queryByType(int type) {
        QueryWrapper<DishCategory> qw = new QueryWrapper<>();
        qw.lambda().eq(DishCategory::getType,type);
        return this.list(qw);
    }



}
