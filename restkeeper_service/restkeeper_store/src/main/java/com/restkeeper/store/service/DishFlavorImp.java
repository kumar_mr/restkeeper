package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.store.entity.DishFlavor;
import com.restkeeper.store.entity.Remark;
import com.restkeeper.store.mapper.DishFlavorMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Service(version = "1.0.0",protocol = "dubbo")
public class DishFlavorImp extends ServiceImpl<DishFlavorMapper, DishFlavor> implements IDishFlavorService{
    @Override
    public boolean physicalDelete(String dishId) {
        return getBaseMapper().physicalDelete(dishId);
    }

    @Override
    public List<DishFlavor> getFlavor(String dishId) {
        QueryWrapper<DishFlavor> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(DishFlavor::getDishId,dishId);
        List<DishFlavor> dishFlavors = this.list(wrapper);
        return dishFlavors;
    }
}
