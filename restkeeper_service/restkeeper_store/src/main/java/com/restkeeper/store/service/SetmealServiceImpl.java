package com.restkeeper.store.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.store.entity.Dish;
import com.restkeeper.store.entity.Setmeal;
import com.restkeeper.store.entity.SetmealDish;
import com.restkeeper.store.mapper.SetmealMapper;
import com.restkeeper.utils.SystemCode;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 套餐服务类
 */
@Service(version = "1.0.0",protocol = "dubbo")
@Component
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements ISetmealService{
    @Autowired
    private ISetmealDishService setmealDishService;


    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean add(Setmeal setmeal, List<SetmealDish> setmealDishes) {
        List<Setmeal> setmealList = this.list();
        List<String> setmealNames=setmealList.stream().map(Setmeal::getName).collect(Collectors.toList());
        if(setmealNames.contains(setmeal.getName())){
            throw new BussinessException("套餐名称重复");
        }
        this.save(setmeal);
        setmealDishes.forEach(s->{
            s.setSetmealId(setmeal.getId());
            s.setIndex(0);
        });
        return setmealDishService.saveBatch(setmealDishes);
    }

    @Override
    public IPage<Setmeal> queryPage(Long pageNo, Long pageSize) {
        QueryWrapper<Setmeal> qw = new QueryWrapper<>();
        qw.orderByDesc("last_update_time");
        IPage<Setmeal> page = new Page<>(pageNo,pageSize);
        return this.page(page,qw);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean delete(List<String> ids) {
        if(ids == null) return false;
        this.removeByIds(ids);
        QueryWrapper<SetmealDish> queryWrapper =new QueryWrapper();
        queryWrapper.lambda().in(SetmealDish::getDishId,ids);
        setmealDishService.remove(queryWrapper);
        return true;
    }

    /**
     * 套餐停售
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean forbiddenSalesStatus(List<String> ids){
        if(ids == null|| ids.isEmpty()) {
            throw new BussinessException("没有需要停售的套餐");
        }
        UpdateWrapper<Setmeal> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().set(Setmeal::getStatus, SystemCode.FORBIDDEN).in(Setmeal::getId, ids);
         return this.update(updateWrapper);
    }

    /**
     * 套餐启售
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean enabledSalesStatus(List<String> ids) {
        if(ids == null|| ids.isEmpty()) {
            throw new BussinessException("没有需要启售的套餐");
        }
        //修改套餐为启售状态
        UpdateWrapper<Setmeal> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().set(Setmeal::getStatus, SystemCode.ENABLED).in(Setmeal::getId, ids);
        return  this.update(updateWrapper);
    }


    @Override
    public long getCountByCategory(String categoryId) {
        QueryWrapper<Setmeal> qw = new QueryWrapper<>();
        qw.lambda()
            .eq(Setmeal::getCategoryId,categoryId);
        return this.count(qw);
    }

    @Override
    public IPage<Setmeal> searchByName(String name, Long pageNo, Long pageSize) {
        QueryWrapper<Setmeal> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotEmpty(name)){
            wrapper.lambda()
                    .like(Setmeal::getName,name);
        }
        IPage<Setmeal> page = new Page<>(pageNo,pageSize);
        return this.page(page,wrapper);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean update(Setmeal setmeal, List<SetmealDish> setmealDishes) {
        //修改setMeal
        this.updateById(setmeal);
        //删除原有的关联菜品
        if (setmealDishes != null || setmealDishes.size()>0){

            QueryWrapper<SetmealDish> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(SetmealDish::getSetmealId,setmeal.getId());
            setmealDishService.remove(queryWrapper);
        }
        //创建新的关联关系
        setmealDishes.forEach((setMealDish)->{
            setMealDish.setSetmealId(setmeal.getId());
        });
       return setmealDishService.saveBatch(setmealDishes);
    }

    //收银端接口
    @Override
    public IPage<Setmeal> queryByCategory(String categoryId, Long pageNo, Long pageSize) {
        IPage<Setmeal> page = new Page<>(pageNo,pageSize);
        QueryWrapper<Setmeal> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Setmeal::getStatus,SystemCode.ENABLED);
        if(!"all".equals(categoryId)){
            wrapper.lambda()
                    .eq(Setmeal::getCategoryId,categoryId);
        }
        return this.page(page,wrapper);
    }
}
