package com.restkeeper.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.store.entity.SellCalculation;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 沽清 Mapper 接口
 * </p>
 */
public interface  SellCalculationMapper extends BaseMapper<SellCalculation> {
    @Select("select free_number from t_sell_calculation where dish_id=#{dishId} where dish_type=1")
    Integer getDishCount(String dishId);
    @Select("select free_number from t_sell_calculation where dish_id=#{setmealId} where dish_type=2")
    Integer getSetmealCount(String setmealId);
}
