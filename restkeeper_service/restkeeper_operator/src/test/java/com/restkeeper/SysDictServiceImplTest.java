package com.restkeeper;

import com.restkeeper.entity.Dictionary;
import com.restkeeper.service.ISysDictService;
import com.restkeeper.utils.SystemCode;
import org.apache.dubbo.config.annotation.Reference;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SysDictServiceImplTest {

    @Reference(version = "1.0.0", check=false)
    ISysDictService sysDictService;

    @Test
    public  void getDictionaryListTest(){
        List<Dictionary> dictionaryList= sysDictService.getDictionaryList(SystemCode.DICTIONARY_REMARK);
        System.out.println(dictionaryList);
    }
}
