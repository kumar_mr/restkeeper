package com.restkeeper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.restkeeper.entity.OperatorUser;

/**
 * <p>
 * 运营端管理员 Mapper 接口
 * </p>
 */
public interface OperatorUserMapper extends BaseMapper<OperatorUser> {

}
