package com.restkeeper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.restkeeper.entity.OperatorUser;
import com.restkeeper.mapper.OperatorUserMapper;
import com.restkeeper.utils.JWTUtil;
import com.restkeeper.utils.MD5CryptUtil;
import com.restkeeper.utils.Result;
import com.restkeeper.utils.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 运营端管理员 服务实现类
 * </p>
 */

@Slf4j
@Service(version = "1.0.0", protocol = "dubbo")
public class OperatorUserServiceImpl extends ServiceImpl<OperatorUserMapper, OperatorUser> implements IOperatorUserService {

    //秘钥
    @Value("${gateway.secret}")
    private String secret;

    @Override
    public IPage<OperatorUser> queryPageByName(int pageNo, int pageSise, String name) {
       IPage<OperatorUser> page = new Page<OperatorUser>(pageNo, pageSise);
        QueryWrapper<OperatorUser> queryWrapper = new QueryWrapper<OperatorUser>();
        if (!StringUtils.isEmpty(name)) {
            queryWrapper.like("loginname", name);
        }
        return this.page(page, queryWrapper);

    }

    @Override
    public Result login(String loginname, String loginpass) {
        Result result = new Result();
        if (StringUtils.isEmpty(loginname)) {
            result.setStatus(ResultCode.error);
            result.setDesc("用户名为空");
            return result;
        }
        if (StringUtils.isEmpty(loginpass)) {
            result.setStatus(ResultCode.error);
            result.setDesc("密码为空");
            return result;
        }
        QueryWrapper<OperatorUser> queryWrapper = new QueryWrapper<OperatorUser>();
        queryWrapper.eq("loginname", loginname);
        OperatorUser user = this.getOne(queryWrapper);
        if (user == null) {
            result.setStatus(ResultCode.error);
            result.setDesc("用户不存在");
            return result;
        }
        String salt = MD5CryptUtil.getSalts(user.getLoginpass());
        if (!Md5Crypt.md5Crypt(loginpass.getBytes(), salt).equals(user.getLoginpass())) {
            result.setStatus(ResultCode.error);
            result.setDesc("密码不正确");
            return result;
        }
        Map<String,Object> tokenMap = new HashMap<>();
        tokenMap.put("userId",user.getUid());
        tokenMap.put("loginName",user.getLoginname());
        String authorization = "";
        try {
            authorization = JWTUtil.createJWTByObj(tokenMap, "" + secret);
        } catch (IOException e) {
            log.error("加密失败", e.getMessage());
            result.setStatus(ResultCode.error);
            result.setDesc("加密失败");
            return result;
        }
        result.setStatus(ResultCode.success);
        result.setDesc("ok");
        result.setData(user);
        result.setAuthorization(authorization);
        return result;
    }
}
