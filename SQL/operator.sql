/*
SQLyog Ultimate v12.3.1 (64 bit)
MySQL - 8.0.18 : Database - restkeeper_operator
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`restkeeper_operator` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `restkeeper_operator`;

/*Table structure for table `t_enterprise_account` */

DROP TABLE IF EXISTS `t_enterprise_account`;

CREATE TABLE `t_enterprise_account` (
  `enterprise_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '企业id',
  `enterprise_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '企业名称',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码（后台自动下发）',
  `shop_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '商户号（下发生成）',
  `applicant` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '手机号（登录账号）',
  `application_time` datetime NOT NULL COMMENT '申请时间（当前时间，精准到分）',
  `expire_time` datetime NOT NULL COMMENT '到期时间 (试用下是默认七天后到期，状态改成停用)',
  `province` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '地址(省)',
  `area` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '地址(区)',
  `city` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '地址(市)',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '详细地址',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态(试用中1，已停用0，正式2)',
  `last_update_time` datetime NOT NULL COMMENT '操作时间',
  `is_deleted` int(11) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`enterprise_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='企业账号管理';

/*Data for the table `t_enterprise_account` */

insert  into `t_enterprise_account`(`enterprise_id`,`enterprise_name`,`password`,`shop_id`,`applicant`,`phone`,`application_time`,`expire_time`,`province`,`area`,`city`,`address`,`status`,`last_update_time`,`is_deleted`) values 
('1199977392401252353','传智播客','$1$fSEzgpOT$uXgwF6YcuyAiRa7Ys1tuX/','f3deb','传智播仔1','18810701234','2020-09-28 22:49:35','2022-10-28 22:49:35','北京市','昌平区','北京市','北京昌平西二旗1111楼',0,'2020-12-09 11:54:36',0),
('1282569564943597569','变成导读','$1$2K8unWJp$dfFgjzE553wI4V0.KCf3f.','lzd4levo','程先','13412344321','2021-07-26 15:38:00','2024-07-25 15:38:00','北京市','东城区','北京市','abc',1,'2021-07-26 15:38:00',0),
('1288655074078302209','多维空间','$1$jAb6H3My$qeXmh6QqsSsEqYIcHd.bB0','yc2peo7n','马特','13261698937','2021-07-26 15:36:22','2021-08-25 15:36:22','北京市','昌平区','北京市','北京北',1,'2021-07-26 15:36:22',0),
('1288658751228813313','鱼文化教育公司','$1$f4CrJu0l$wJk7/pV/ZrN5xxXBx4MWk1','i2vhpfjd','播仔','13301131116','2020-10-04 23:08:30','2020-10-11 23:08:30','北京市','昌平区','北京市','建材城',0,'2021-07-26 15:33:19',0),
('1288724430606901250','英急立教育','$1$HxovnSkH$FREd2ncwEL0CAgVleqNJr.','bqlkj3j4','魏朝辉','15663692227','2021-07-26 15:37:16','2076-04-28 15:37:16','北京市','东城区','北京市','北京市',1,'2021-07-26 15:37:16',0),
('1290131311820857345','文学教育','$1$dbM5b4Dq$a5mBt3nubzVaQ5LtyuHWx1','hwqdedyt','汤大友','12323232424','2021-07-26 15:41:51','2021-08-25 15:41:51','天津市','河东区','天津市','23123123',1,'2021-07-26 15:41:51',0),
('1290173480002707458','果香木食品','$1$eQSb6UJy$PXeu7elz4OcCHPV1EYuw5.','oejmvpy9','申工','18723423423','2021-07-26 15:40:00','2021-08-25 15:40:00','天津市','河东区','天津市','123',1,'2021-07-26 15:40:00',0),
('1290173758131200001','抹油公司','$1$Ip/we3jU$JRpVJHT9EDTC1CoxyWdIE0','16q3zh9j','李波','18723422342','2020-08-20 17:27:54','2020-09-19 17:27:54','天津市','河东区','天津市','是否',-1,'2020-10-06 12:59:09',0),
('1290459624883937281','黑马博客在培训教育公司','$1$AlqIKj8J$N0Cv.O/.LnZNqS7nAiv5v0','nounavcd','郝大友','13261698937','2021-07-26 15:31:42','2023-07-26 15:31:42','北京市','东城区','北京市','北京',1,'2021-07-26 15:31:42',0),
('1290487095578648578','新潮服饰','$1$R2OSnQzO$nE6arUKVd6B/EoZlcKUom/','45btjj2j','张飒','17897898987','2021-07-26 15:40:47','2021-08-25 15:40:47','河北省','秦皇岛市','河北省','撒地方',1,'2021-07-26 15:40:47',0),
('1292743089687945217','北京企业','$1$S105u0bc$5eXUOq166RQvB2Lc5Boa11','jogjcts5','沈亥','18200000000','2021-07-26 15:41:08','2021-08-25 15:41:08','北京市','东城区','北京市','东城',1,'2021-07-26 15:41:08',0),
('1310597706731769858','峨眉旅游文化公司','$1$hC/SI5wj$WK0hicmZZa1HvC2DQ.AXS/','gi9dwoon','长白山','13758010848','2020-10-04 22:31:00','2020-10-11 22:31:00','天津市','河西区','天津市','111',-1,'2021-07-26 15:30:44',0),
('1312767355649019905','百姓科技公司','$1$O1OAN01M$uLWIvHC74nc7HJRX6mUq51','fxkuzn3w','欧阳奈','13758010848','2020-10-04 22:51:42','2021-10-04 22:51:42','北京市','西城区','北京市','222',-1,'2021-07-26 15:32:47',0),
('1313296756748865538','孔轩文化公司','$1$Z8ZhNvAo$x/NysQL.J532g2XqvmVZ51','4zdkcyhh','孔宣','1452658872','2020-10-06 09:55:21','2020-10-13 09:55:21','北京市','东城区','北京市','1231231',0,'2021-07-26 15:35:54',0),
('1313390061058805761','啊婆汤企业','$1$R4cFYtsw$vvm0FMBbQUB73Csvo/Jf.0','nqye1qqb','吴用','19245874612','2020-10-06 16:06:07','2020-10-13 16:06:07','辽宁省','丹东市','辽宁省','12312',-1,'2021-07-26 15:35:22',0),
('1313430799456948226','先数教育','$1$rTDRo.D4$fgEHJO1E3LL73rJc1Vici0','jwcqwjxv','童松','18645872113','2020-10-06 18:48:00','2020-10-13 18:48:00','山西省','阳泉市','山西省','aada',0,'2021-07-26 15:34:24',0),
('1314040470769258498','嵩山少林文化旅游公司','$1$u0RyNTND$UYCZt2qs71n/wzbpHedao/','demtd9kp','卢大友','13758010848','2020-11-18 10:13:25','2020-11-28 10:13:25','北京市','西城区','北京市','string',0,'2021-07-26 15:28:43',0),
('1330833161339355138','山东水浒旅游文化公司','$1$CbwPWEIo$tB55phQGM07UJSjEUO2Qz/','ma8jq795','宋江','17522674851','2020-11-23 19:18:46','2020-11-30 19:18:46','北京市','西城区','北京市','12',-1,'2021-07-26 15:27:41',0);

/*Table structure for table `t_operator_user` */

DROP TABLE IF EXISTS `t_operator_user`;

CREATE TABLE `t_operator_user` (
  `uid` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户id',
  `loginname` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '鐢ㄦ埛鍚?',
  `loginpass` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '鐢ㄦ埛瀵嗙爜',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='运营端管理员';

/*Data for the table `t_operator_user` */

insert  into `t_operator_user`(`uid`,`loginname`,`loginpass`) values 
('1','test','test'),
('2c50858d9f5b61fd89ef599e27395207','admin','$1$RCXPkN60$IvVVIK7GGHnoo7BdxmfZl0');

/*Table structure for table `t_sys_dict` */

DROP TABLE IF EXISTS `t_sys_dict`;

CREATE TABLE `t_sys_dict` (
  `dict_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '数据id',
  `dict_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '字典名称',
  `dict_data` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '内容',
  `category` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '所属分类',
  `info` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '信息',
  `last_update_time` timestamp NULL DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`dict_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='全局字典表';

/*Data for the table `t_sys_dict` */

insert  into `t_sys_dict`(`dict_id`,`dict_name`,`dict_data`,`category`,`info`,`last_update_time`) values 
('1193707623406579714','甜味','[好甜，太甜]','flavor','口味描述','2020-02-04 17:09:05'),
('1193707623406579715','退菜原因','[上菜太慢]','remark','备注信息','2020-02-04 17:09:08'),
('1193707623406579716','打折原因','[店庆，节假日]','remark','备注信息','2020-02-04 17:09:10');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
