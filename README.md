# 餐掌柜-SAAS餐饮平台项目

## 项目介绍

​		餐掌柜是互联网餐厅 SaaS 服务及运营平台，通过B端SaaS服务和C端产品，解决餐厅的效率、管理、营销、成本和顾客就餐体验等问题。餐掌柜分为三个模块，运营中心、管家端和收银端。

​	    餐掌柜运营中心是平台端用于管理企业账号的管理后台，可以对企业账号进行新增、修改、禁用、开启等操作。

​		餐掌柜支持集团餐饮企业业务。餐掌柜管理端分为两个入口，集团管理入口和门店管理入口。集团管理入口，是餐饮集团用于管理品牌和门店的管理后台，功能包括品牌管理、门店管理和店长管理。 门店管理入口是餐饮门店用于日常管理的管理后台，包括区域与桌台管理、分类管理、菜品管理、套餐管理、员工管理等功能。

​		餐掌柜收银端是餐饮门店用于日常收银的子系统，主要功能包括开桌、下单、结算收银等功能。

## 相关概念

**品牌：**

​       一个餐饮集团下可以经营多个餐饮品牌。

**门店：**

​        一个餐饮集团的某个品牌下可以有多个门店，或称为分店。

**区域：**

​       一个门店有很多桌台，而区域就是桌台的分组名称。例如一层大厅，二层大厅，二层包间等。

**套餐：**

​       一个套餐下包括多个菜品。

## 体验入口

官网：

https://pip.itcast.cn/eatManager

运营端：

http://canzg-wsl.itheima.net/opt/#/login?redirect=%2Fmember

管理端：

http://canzg-wsl.itheima.net/web/#/login

收银端：

![](assets/1.png)


## 产品原型

运营端 

https://app.mockplus.cn/run/prototype/7Y7D8QJvj-4q/UBODd_r_1eEA?ha=1&linkID=I6PSTZd1zcxI&ps=1 

管家端  

https://app.mockplus.cn/run/prototype/7Y7D8QJvj-4q/-8oYYRTC0uZ9/wEBRPYZaZyWXZl?ha=1&linkID=5qwgphnfNG5n&ps=1

收银端 

https://app.mockplus.cn/run/prototype/7Y7D8QJvj-4q/XPS24ssrYqJ/tYmS9NHhJNwj?ha=1&linkID=MGNokSltRwqT&ps=1 

## 技术架构

系统主要采用SpringCloudAlibaba框架，持久层框架采用mybatisPlus。

![](assets/1-8.png)

## 系统结构

系统结构说明：安装说明

| 工程模块名称                     | 作用               |
| -------------------------------- | ------------------ |
| restkeeper_common                | 公共模块           |
| restkeeper_gateway               | 网关模块           |
| restkeeper_service（二级父工程） | 微服务业务层       |
| restkeeper_operator              | 运营端业务模块     |
| restkeeper_operator_api          | 运营端业务接口模块 |
| restkeeper_order                 | 订单业务模块       |
| restkeeper_order_api             | 订单业务接口模块   |
| restkeeper_shop                  | 集团业务模块       |
| restkeeper_shop_api              | 集团业务接口模块   |
| restkeeper_store                 | 门店业务模块       |
| restkeeper_store_api             | 门店业务接口模块   |
| restkeeper_service_common        | 业务层公共模块     |
| restkeeper_web（二级父工程）     | 微服务表现层       |
| restkeeper_operator_web          | 运营端web模块      |
| restkeeper_enterprice_web        | 管家端web模块      |
| restkeeper_cash_web              | 收银端web模块      |
| web_common                       | web层公共模块      |

### 版本说明

按照以下版本信息搭建环境

| 软件          | 版本   |
| ------------- | ------ |
| JDK           | 8      |
| MySQL         | 8.0.20 |
| nacos         | 1.1.4  |
| redis         | 6.0.5  |
| elasticsearch | 7.7.1  |
| logstash      | 7.7.1  |

### 环境安装

（1）安装mysql

```yaml
#下载Mysql8镜像
docker pull mysql:8.0.18
#创建mysql8容器
docker run --name mysqlserver -v $PWD/conf:/etc/mysql/conf.d -v $PWD/logs:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d -i -p 3306:3306 mysql:8.0.18
```

（2）安装redis

```yaml
#下载redis镜像
docker pull redis
#创建redis容器
docker run -p 6379:6379 --name redis -v /usr/local/docker/redis.conf:/etc/redis/redis.conf -v /usr/local/dockr/data:/data -d redis redis-server /etc/redis/redis.conf --appendonly yes
```

（3）安装nacos

```yaml
#下载nacos镜像
docker pull nacos/nacos-server:1.1.4
#创建nacos容器
docker run --env MODE=standalone --name nacos -d -p 8848:8848 nacos/nacos-server:1.1.4
```

### 环境配置

#### 导入nacos配置文件

在nacos中导入源码中NacocConfig目录中nacos_config_export_2021-10-21 10_12_55.zip压缩包。

#### mysql建库建表

源码中SQL目录下提供了5个SQL文件，用于创建数据表结构和导入测试数据。

- 导入operator.sql建表
- 导入shop.sql建表
- 导入store.sql建表
- 导入order.sql建表
- 导入seata.sql建表

### 项目运行

下载餐掌柜前端资源：

链接：https://pan.baidu.com/s/1Kp3rj4siDEHE7J2K_tQl7g 
提取码：8888

#### 运行餐掌柜运营中心

（1）依次启动restkeeper_operator 、restkeeper_operator_web和restkeeper_gateway。

（2）在下载的餐掌柜前端资源中“运营端与管家端”目录找到nginx.zip并解压，注意不要使用中文目录。执行nginx.exe即可。

（3）打开浏览器，输入 http://localhost   即可打开餐掌柜运营中心。

#### 运行餐掌柜管家端

（1）依次启动restkeeper_operator 、restkeeper_shop、restkeeper_store、restkeeper_order、restkeeper_enterprise_web和restkeeper_gateway。

（2）在下载的餐掌柜前端资源中“运营端与管家端”目录找到nginx.zip并解压，注意不要使用中文目录。执行nginx.exe即可。

（3）打开浏览器，输入 http://localhost:90   即可打开餐掌柜管家端。

#### 运行收银端

（1）依次启动restkeeper_store、 restkeeper_order、restkeeper_cash_web以及lkd_gate_way 。

（2）安装mumu模拟器 。在下载的餐掌柜前端资源中已提供：收银端/MuMuInstaller_1.1.0.8_nochannel_zh-Hans_1598240340.exe

（3） 我们在这里将餐掌柜前端资源中的czg.apk 安装到mumu模拟器中。

（4） 双击“餐掌柜” 进入餐掌柜系统。

（5） 点击登录界面的“收银系统”，在谈出的窗口中输入地址  http://ip:8085/cash   注意：这里的ip是本地的ip地址，不能输入localhost或127.0.0.1 ， 必须通过ipconfig获得。

## 库表结构

### 运营中心库restkeeper_operator

| 表名称               | 表含义           |
| -------------------- | ---------------- |
| t_enterprise_account | 企业账号表       |
| t_operator_user      | 运营中心管理员表 |
| t_sys_dict           | 字典表           |

### 集团库restkeeper_shop

| 表名称          | 表含义   |
| --------------- | -------- |
| t_brand         | 品牌名称 |
| t_store         | 门店表   |
| t_store_manager | 店长表   |

### 门店库restkeeper_store

| 表名称            | 表含义         |
| ----------------- | -------------- |
| t_category        | 分类表         |
| t_dish            | 菜品表         |
| t_dish_flavor     | 菜品口味表     |
| t_setmeal         | 套餐表         |
| t_setmeal_dish    | 套餐菜品设置表 |
| t_payment_setting | 支付设置表     |
| t_printer         | 打印机表       |
| t_printer_dish    | 打印菜品设置表 |
| t_remark          | 备注设置表     |
| t_staff           | 员工表         |
| t_staff_rank      | 员工等级表     |
| t_table           | 桌台表         |
| t_table_area      | 桌台区域表     |
| t_table_log       | 桌台日志       |

### 订单库restkeeper_order

| 表名称              | 表含义                     |
| ------------------- | -------------------------- |
| t_order             | 订单表                     |
| t_order_detail      | 订单明细表                 |
| t_order_detail_meal | 订单明细表（套餐菜品明细） |
| t_his_order         | 历史订单表                 |
| t_his_order_detail  | 历史订单明细表             |
| t_report_dish       | 菜品销售聚合统计表         |
| t_report_pay        | 销售支付方式聚合统计表     |
| t_report_time       | 销售时间段聚合统计表       |
| t_reverse_order     | 反结账表                   |

