package com.restkeeper.vo;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ReceiptVO {

    private long page;
    private long pageSize;
    private String startDate;
    private String endDate;
    private String orderNumber;
    private String operatorName;
    private Integer payType;


}
