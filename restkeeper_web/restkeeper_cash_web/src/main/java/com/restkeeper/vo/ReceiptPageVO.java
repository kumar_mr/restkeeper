package com.restkeeper.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.restkeeper.response.page.PageVO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReceiptPageVO<T> extends PageVO<T> {

    private long alltotalAmount=0;
    private long allPayAmount=0;

    public ReceiptPageVO(IPage page) {
        super(page);
    }
}
