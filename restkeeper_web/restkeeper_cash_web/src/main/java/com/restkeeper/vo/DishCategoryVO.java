package com.restkeeper.vo;

import lombok.Data;

@Data
public class DishCategoryVO {
    private String categoryId;
    private String name;
    private Integer type;
}
