package com.restkeeper.vo;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 下单订单vo
 */
@Data
public class OrderVO {
    @ApiModelProperty(value = "订单id")
    private String orderId;
    @ApiModelProperty(value = "桌台号")
    private String tableId;
    @ApiModelProperty(value = "流水号")
    private String orderNumber;
    @ApiModelProperty(value = "就餐人数")
    private Integer personNumbers;
    @ApiModelProperty(value = "总金额")
    private Integer totalAmount;
    @ApiModelProperty(value = "付款金额")
    private Integer payAmount;
    @ApiModelProperty(value = "整单备注")
    private List<String> orderRemark = Lists.newArrayList();
    @ApiModelProperty(value = "赠菜金额")
    private  Integer presentAmount;
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "订单菜品列表")
    private List<OrderDetailVO> dishs;

}

