package com.restkeeper.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 结账菜品赠菜，退菜封装
 */
@Data
public class PayDetailVO {

    @ApiModelProperty(value = "菜品id")
    private String dishId;
    @ApiModelProperty(value = "备注")
    private String remark;
}
