package com.restkeeper.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.restkeeper.response.page.PageVO;
import com.restkeeper.store.entity.SellCalculation;
import com.restkeeper.store.service.ISellCalculationService;
import com.restkeeper.vo.SellCalculationVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = {"沽算业务"})
@RestController
@RequestMapping("/sellcalculation")
public class SellCalculationController {

    @Reference(version = "1.0.0",check = false)
    private ISellCalculationService sellCalculationService;

    /**
     * 添加估清
     * @param sellCalculationVO
     * @return
     */
    @ApiOperation(value = "添加估清")
    @PostMapping("/add")
    public boolean add(@RequestBody SellCalculationVO sellCalculationVO){
        SellCalculation sellCalculation =new SellCalculation();
        sellCalculation.setDishId(sellCalculationVO.getDishId());
        sellCalculation.setDishType(sellCalculationVO.getDishType());
        sellCalculation.setSellLimitTotal(sellCalculationVO.getNumbers());
        sellCalculation.setRemainder(sellCalculationVO.getNumbers());
        sellCalculation.setDishName(sellCalculationVO.getDishName());
        return sellCalculationService.save(sellCalculation);
    }

    @ApiOperation(value = "查询分页数据")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType="query", name = "page", value = "当前页码", required = false, dataType = "Long"),
            @ApiImplicitParam(paramType="query", name = "pagesize", value = "分大小", required = false, dataType = "Long")
    })
    @GetMapping(value = "/pageList/{page}/{pageSize}")
    public PageVO<SellCalculation> findListByPage(@PathVariable Long page, @PathVariable Long pageSize){
        IPage<SellCalculation> pageResult = new Page<>(page, pageSize);
        return new PageVO<SellCalculation>(sellCalculationService.page(pageResult));
    }

    @ApiOperation(value = "估清删除")
    @DeleteMapping("/delete")
    public boolean delete(@RequestBody List<String> ids){
        return sellCalculationService.removeByIds(ids);
    }


}
