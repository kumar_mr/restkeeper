package com.restkeeper.controller.store;

import com.restkeeper.response.page.PageVO;
import com.restkeeper.store.entity.DishCategory;
import com.restkeeper.store.service.IDishCategoryService;
import com.restkeeper.store.service.IDishService;
import com.restkeeper.store.service.ISetmealService;
import com.restkeeper.vo.store.AddCategoryVO;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@Api(tags = { "分类管理" })
@RestController
@RequestMapping("/dishCategory")
public class DishCategoryController{
    @Reference(version = "1.0.0", check=false)
    private IDishCategoryService dishCategoryService;
    @Reference(version = "1.0.0", check=false)
    private IDishService dishService;
    @Reference(version = "1.0.0", check=false)
    private ISetmealService setmealService;

    /**
     * 添加分类
     * @param addCategoryVO
     * @return
     */
    @ApiOperation(value = "添加分类")
    @PostMapping("/add")
    public boolean add(@RequestBody AddCategoryVO addCategoryVO){
        return dishCategoryService.add(addCategoryVO.getCategoryName(),addCategoryVO.getType());
    }

    /**
     * 菜品，套餐分类下拉列表使用
     * 1 菜品 2 套餐
     * @return
     */
    @ApiOperation(value = "获取分类")
    @GetMapping("/type/{type}")
    public List<Map<String,Object>> getByType(@PathVariable Integer type){
        return dishCategoryService.findCategoryList(type);
    }

    /**
     * 修改分类
     * @return
     */
    @ApiOperation(value = "修改分类")
    @PutMapping("/update/{id}")
    public boolean update(@PathVariable String id, @RequestParam(value = "categoryName") String categoryName){
        return dishCategoryService.update(id, categoryName);
    }

    /**
     * @param id
     * @return
     */
    @ApiOperation(value = "删除分类")
    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable String id){
        return dishCategoryService.delete(id);
    }

    @ApiOperation(value = "分类列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "page", value = "当前页码", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "path", name = "pageSize", value = "分页大小", required = true, dataType = "Long")})
    @GetMapping("/pageList/{page}/{pageSize}")
    public PageVO<DishCategory> findByPage(@PathVariable Long page, @PathVariable Long pageSize){
        return new PageVO<>(dishCategoryService.queryPage(page,pageSize));
    }
}
