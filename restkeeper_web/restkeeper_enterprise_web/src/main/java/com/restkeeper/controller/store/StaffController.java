package com.restkeeper.controller.store;


import com.restkeeper.response.page.PageVO;
import com.restkeeper.store.entity.Staff;
import com.restkeeper.store.service.IStaffService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"员工管理"})
@RestController
@RequestMapping("/staff")
public class StaffController {

    @Reference(version = "1.0.0", check=false)
    private IStaffService staffService;

    /**
     * 根据id获取员工
     */
    @ApiOperation(value = "根据id获取员工")
    @GetMapping(value = "/{id}")
    public Staff getById(@PathVariable String id){
       return staffService.getById(id);
    }


    /**
     * 新增员工
     */
    @ApiOperation(value = "新增员工")
    @PostMapping(value = "/add")
    public boolean add(@RequestBody Staff staff){
        return staffService.addStaff(staff);
    }

    @ApiOperation(value = "修改员工")
    @PutMapping("/update")
    public boolean update(@RequestBody Staff staff){
        return staffService.updateStaff(staff);
    }

    /**
     * 员工搜索
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @ApiOperation(value = "根据用户名称分页搜索")
    @GetMapping("/pageList/{page}/{pageSize}")
    public PageVO<Staff> pageList(@PathVariable long page,@PathVariable long pageSize,@RequestParam(name="name",defaultValue = "")String name){
        return new PageVO<Staff>(staffService.search(page,pageSize,name));
    }

    /**
     * 启用/禁用
     * @param id
     * @param status
     * @return
     */
    @ApiOperation(value = "启用/禁用 用户信息")
    @PutMapping("/{id}/{status}")
    public boolean updateStatus(@PathVariable String id, @PathVariable int status){
        Staff staff = staffService.getById(id);
        staff.setStatus(status);
        return staffService.updateById(staff);
    }
}
