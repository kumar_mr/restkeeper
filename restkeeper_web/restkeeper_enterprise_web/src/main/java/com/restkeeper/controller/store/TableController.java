package com.restkeeper.controller.store;

import com.restkeeper.exception.BussinessException;
import com.restkeeper.response.page.PageVO;
import com.restkeeper.shop.service.IStoreService;
import com.restkeeper.store.entity.Table;
import com.restkeeper.store.entity.TableArea;
import com.restkeeper.store.service.ITableAreaService;
import com.restkeeper.store.service.ITableService;
import com.restkeeper.utils.QRCodeUtil;
import com.restkeeper.vo.store.TableVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Base64;

@Api(tags = { "桌台管理" })
@RestController
@RequestMapping("/table")
public class TableController{

    @Reference(version = "1.0.0", check=false)
    private ITableService tableService;
    @Reference(version = "1.0.0", check=false)
    private ITableAreaService tableAreaService;

    @Reference(version = "1.0.0", check=false)
    private IStoreService storeService;

    @Autowired
    private HttpServletResponse response;

    @Value("${QRCode.host:''}")
    private String qrCodeHost;


    @GetMapping("/areaPageList/{page}/{pageSize}")
    public PageVO<TableArea> areaPageList(@PathVariable long page,@PathVariable long pageSize){
        return new PageVO<TableArea>(tableAreaService.queryPage(page,pageSize));
    }


    @ApiOperation(value = "根据区域id检索桌台")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "areaId", value = "区域Id", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "path", name = "page", value = "页码", required = true, dataType = "Long"),
            @ApiImplicitParam(paramType = "path", name = "pageSize", value = "每页数量", required = true, dataType = "Long")})
    @GetMapping("/search/{areaId}/{page}/{pageSize}")
    public PageVO<Table> queryByArea(@PathVariable String areaId,@PathVariable Long page,@PathVariable Long pageSize){
        return new PageVO<Table>(tableService.queryPageByArea(areaId,page,pageSize));
    }

    @ApiOperation(value = "获取桌台详情")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "tableId", value = "桌台Id", required = true, dataType = "String")})
    @GetMapping("/{tableId}")
    public Table getById(@PathVariable String tableId){
        return tableService.getById(tableId);
    }

    @ApiOperation(value = "添加桌台")
    @PostMapping("/add")
    public boolean add(@RequestBody TableVO tableVo){
        Table table = new Table();
        table.setTableName(tableVo.getName());
        table.setSeatNumber(tableVo.getSeatNumber());
        table.setAreaId(tableVo.getAreaId());
        // 0空闲 1 开桌 2 锁桌
        table.setStatus(0);
        return tableService.add(table);
    }

    @ApiOperation(value = "更新桌台")
    @PutMapping("/update/{id}")
    public boolean update(@PathVariable String id, @RequestBody TableVO tableVO){
        Table table = tableService.getById(id);
        table.setAreaId(tableVO.getAreaId());
        table.setSeatNumber(tableVO.getSeatNumber());
        table.setTableName(tableVO.getName());
        return tableService.updateById(table);
    }

    @ApiOperation(value = "删除桌台")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "tableId", value = "桌台Id", required = true, dataType = "String")})
    @DeleteMapping("/delete/{tableId}")
    public boolean delete(@PathVariable String tableId){
        return tableService.removeById(tableId);
    }

    @ApiOperation(value = "删除区域")
    @DeleteMapping("/deleteArea/{id}")
    public boolean deleteArea(@PathVariable String id){
        return tableAreaService.deleteArea(id);
    }

    @ApiOperation(value = "添加区域")
    @PostMapping("/addArea")
    public boolean addArea(@RequestParam("name") String name){
        TableArea tableArea = new TableArea();
        tableArea.setAreaName(name);
        return tableAreaService.add(tableArea);
    }

    @ApiOperation(value = "更新区域")
    @PutMapping("/updateArea/{id}")
    public boolean updateArea(@PathVariable String id, @RequestParam("name") String name){
        TableArea tableArea = tableAreaService.getById(id);
        tableArea.setAreaName(name);
        return tableAreaService.updateById(tableArea);
    }



    @GetMapping("/getQRCode/{tableId}")
    public String getQRCode(@PathVariable String tableId){
        Table table = tableService.getById(tableId);
//        //获取门店品牌
//        Store store =storeService.getById(table.getStoreId());
//        Brand brand= store.getBrand();
//        String logon = brand.getLogo();
        String text=qrCodeHost+"?shopId="+table.getShopId()+"&storeId="+table.getStoreId()+"&tableId="+table.getTableId();
        try {
            BufferedImage image = QRCodeUtil.createImage(text, "");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (!ImageIO.write(image, "jpg", baos)) {
                throw new BussinessException("Could not write an image of format jpg");
            }
            return "data:image/png;base64," + Base64.getEncoder().encodeToString(baos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 清台
     * @param tableId
     * @return
     */
    @GetMapping("/clean/{tableId}")
    public boolean cleanTable(@PathVariable String tableId){
        return tableService.cleanTable(tableId);
    }
}
