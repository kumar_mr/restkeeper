package com.restkeeper.controller.store;


import ch.qos.logback.core.joran.util.beans.BeanUtil;
import com.restkeeper.exception.BussinessException;
import com.restkeeper.store.entity.Remark;
import com.restkeeper.store.service.IRemarkService;
import com.restkeeper.utils.BeanListUtils;
import com.restkeeper.vo.store.SettingsVO;
import com.restkeeper.vo.store.RemarkVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Api(tags = { "门店设置" })
@RestController
@RequestMapping("/settings")
public class SettingsController {

    @Reference(version = "1.0.0", check=false)
    private IRemarkService remarkService;

    @ApiOperation(value = "获取备注信息")
    @GetMapping("/getSysSettings")
    public SettingsVO getSysSettings(){
        List<Remark> remarks= remarkService.getRemarks();
        List<RemarkVO> remarkvos= new ArrayList<>();
        remarks.forEach(d->{
            RemarkVO remarkVO=new RemarkVO();
            remarkVO.setRemarkName(d.getRemarkName());
            String remarkValue = d.getRemarkValue();
            String remarkValue_substring=remarkValue.substring(remarkValue.indexOf("[")+1,remarkValue.indexOf("]"));
            if(StringUtils.isNotEmpty(remarkValue_substring)){
                String[] remark_array= remarkValue_substring.split(",");
                remarkVO.setRemarkValue(Arrays.asList(remark_array));
            }
            remarkvos.add(remarkVO);
        });
        SettingsVO settingsVO =new SettingsVO();
        settingsVO.setRemarks(remarkvos);
        return settingsVO;
    }

    @ApiOperation(value = "修改门店设置")
    @PutMapping("/update")
    public boolean update(@RequestBody SettingsVO settingsVO){
        //当前只有门店备注
        List<RemarkVO> remarkList= settingsVO.getRemarks();
        List<Remark> remarks= new ArrayList<Remark>();
        for (RemarkVO remarkVO : remarkList) {
            Remark remark =new Remark();
            remark.setRemarkName(remarkVO.getRemarkName());
            remark.setRemarkValue(remarkVO.getRemarkValue().toString());
            remarks.add(remark);
        }
       return remarkService.updateRemarks(remarks);
    }
}
