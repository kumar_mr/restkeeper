package com.restkeeper.controller.store;

import com.restkeeper.response.page.PageVO;
import com.restkeeper.store.entity.Printer;
import com.restkeeper.store.service.IPrinterService;
import com.restkeeper.vo.store.PrinterVO;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/printer")
public class PrinterController{
    @Reference(version = "1.0.0", check=false)
    private IPrinterService printerService;

    @GetMapping("/pageList/{page}/{pageSize}")
    public PageVO<Printer> queryPage(@PathVariable long page, @PathVariable long pageSize){
        return new PageVO<>(printerService.queryPage(page,pageSize));
    }

    @PostMapping
    public boolean AddPrinter(@RequestBody PrinterVO printerVO){
        Printer printer = new Printer();
        printer.setAreaType(printerVO.getAreaType());
        printer.setEnableBeforehand(printerVO.isEnableBeforehand());
        printer.setEnableBill(printerVO.isEnableBill());
        printer.setEnableChangeMenu(printerVO.isEnableChangeMenu());
        printer.setEnableChangeTable(printerVO.isEnableChangeTable());
        printer.setEnableCustomer(printerVO.isEnableCustomer());
        printer.setEnableMadeMenu(printerVO.isEnableMadeMenu());
        printer.setEnableReturnDish(printerVO.isEnableReturnDish());
        printer.setMachineCode(printerVO.getMachineCode());
        printer.setPrinterName(printerVO.getPrinterName());
        printer.setPrinterNumber(printerVO.getPrinterNumber());
        if(printerVO.getAreaType() == 1){
            return printerService.addBackendPrinter(printer,printerVO.getDishIdList());
        }else {
            return printerService.addFrontPrinter(printer);
        }
    }

    @PutMapping
    public boolean update(@RequestBody PrinterVO printerVO){
        Printer printer = printerService.getById(printerVO.getPrinterId());
        printer.setAreaType(printerVO.getAreaType());
        printer.setEnableBeforehand(printerVO.isEnableBeforehand());
        printer.setEnableBill(printerVO.isEnableBill());
        printer.setEnableChangeMenu(printerVO.isEnableChangeMenu());
        printer.setEnableChangeTable(printerVO.isEnableChangeTable());
        printer.setEnableCustomer(printerVO.isEnableCustomer());
        printer.setEnableMadeMenu(printerVO.isEnableMadeMenu());
        printer.setEnableReturnDish(printerVO.isEnableReturnDish());
        printer.setMachineCode(printerVO.getMachineCode());
        printer.setPrinterName(printerVO.getPrinterName());

        return printerService.updatePrinter(printer,printerVO.getDishIdList());
    }

    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable Long id){
        return printerService.removeById(id);
    }
}
