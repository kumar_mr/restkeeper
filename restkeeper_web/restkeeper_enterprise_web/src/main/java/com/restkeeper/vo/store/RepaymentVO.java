package com.restkeeper.vo.store;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 还款VO
 */
@Data
public class RepaymentVO {
    @ApiModelProperty(value = "挂账订单id")
    private int id;
    @ApiModelProperty(value = "挂账类型 1 企业 2 个人")
    private int creditType;
    @ApiModelProperty(value = "挂账人")
    private String creditName;
    @ApiModelProperty(value = "挂账金额")
    private String creditAmount;
    @ApiModelProperty(value = "挂账金额 ")
    private Integer payType; // 1 现金 2 微信 3 支付宝  4 银行卡
    @ApiModelProperty(value = "还款金额 ")
    private Integer repaymentAmount; // 还款金额
}



