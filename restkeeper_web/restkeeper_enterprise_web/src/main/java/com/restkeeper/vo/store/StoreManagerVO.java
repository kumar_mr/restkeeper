package com.restkeeper.vo.store;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
@Data
public class StoreManagerVO{
    @ApiModelProperty(value = "门店管理员id")
    private String storeManagerId;
    @ApiModelProperty(value = "门店管理员姓名")
    private String storeManagerName;
    @ApiModelProperty(value = "门店管理员电话")
    private String storeManagerPhone;
    @ApiModelProperty(value = "门店id列表")
    private List<String> storeIds;
}
