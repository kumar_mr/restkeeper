package com.restkeeper.vo.shop;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateStoreVO extends AddStoreVO {
	@ApiModelProperty(value = "门店主键id")
	private String storeId;
}
