package com.restkeeper.response.exception;

import com.restkeeper.exception.BussinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 拦截异常并转换成 自定义ExceptionResponse
 */
@RestControllerAdvice
@Slf4j
public class RestKeeperExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Object Exception(Exception ex) {
        log.error("http response error",ex);
        ExceptionResponse response =new ExceptionResponse(ex.getMessage());
        return response;
    }

    @ExceptionHandler(BussinessException.class)
    public Object Exception(BussinessException ex) {
        log.error("http response error",ex);
        ExceptionResponse response =new ExceptionResponse(ex.getMessage());
        return response;
    }
}
